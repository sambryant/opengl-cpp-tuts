#include "floor.hpp"

GLfloat Floor::color_data[] = {
  0.2f, 0.2f, 0.2f,
  0.2f, 0.2f, 0.2f,
  0.2f, 0.2f, 0.2f,
  0.2f, 0.2f, 0.2f,
  0.2f, 0.2f, 0.2f,
  0.2f, 0.2f, 0.2f
};

GLfloat Floor::vertex_data[] = {
  -10.0f, 0.0f, -10.0f,
   10.0f, 0.0f,  10.0f,
   10.0f, 0.0f, -10.0f,
  -10.0f, 0.0f, -10.0f,
   10.0f, 0.0f,  10.0f,
  -10.0f, 0.0f,  10.0f
};

GLfloat* Floor::get_vertex_data() {
  return Floor::vertex_data;
}

GLfloat* Floor::get_color_data() {
  return Floor::color_data;
}
