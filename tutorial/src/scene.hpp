#ifndef SCENE_H
#define SCENE_H
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include <GL/glew.h>

#include "object.hpp"

#define MAX_VERTICES 65536

using namespace std;

class Scene {
private:
  GLuint modelID;
  int vertex_position;
public:
  vector<Object*> objects;
  GLfloat g_vertex_buffer_data[MAX_VERTICES];
  GLfloat g_color_buffer_data[MAX_VERTICES];

  Scene(GLuint modelID) {
    this->vertex_position = 0;
    this->modelID = modelID;
  }

  void draw() {
    for (int i=0; i<objects.size(); i++) {
      objects[i]->draw(this->modelID);
    }
  }

  void add(Object* object) {
    this->vertex_position += object->write_vertex_data(
      this->vertex_position,
      this->g_vertex_buffer_data,
      this->g_color_buffer_data);
    this->objects.push_back(object);
  }
};

#endif