#include "tetrahedron.hpp"

glm::vec4 Tetrahedron::points[] = {
  glm::vec4(+0.0f     ,  -0.36014829f,  1.0f, 1.0f),
  glm::vec4(+0.866025f,  -0.36014829f, -0.5f, 1.0f),
  glm::vec4(-0.866025f,  -0.36014829f, -0.5f, 1.0f),
  glm::vec4(      0.0f,  +1.06287666f,  0.0f, 1.0f)
};

GLfloat Tetrahedron::color_data[] = {
  1.0f, 0.0f, 0.0f,
  1.0f, 0.0f, 0.0f,
  1.0f, 0.0f, 0.0f,
  1.0f, 0.0f, 1.0f,
  1.0f, 0.0f, 1.0f,
  1.0f, 0.0f, 1.0f,
  1.0f, 0.2f, 0.5f,
  1.0f, 0.2f, 0.5f,
  1.0f, 0.2f, 0.5f,
  0.2f, 0.5f, 1.0f,
  0.2f, 0.5f, 1.0f,
  0.2f, 0.5f, 1.0f,
};

GLfloat Tetrahedron::vertex_data[] = {
  points[0][0], points[0][1], points[0][2],
  points[1][0], points[1][1], points[1][2],
  points[2][0], points[2][1], points[2][2],

  points[3][0], points[3][1], points[3][2],
  points[1][0], points[1][1], points[1][2],
  points[0][0], points[0][1], points[0][2],

  points[1][0], points[1][1], points[1][2],
  points[3][0], points[3][1], points[3][2],
  points[2][0], points[2][1], points[2][2],

  points[2][0], points[2][1], points[2][2],
  points[0][0], points[0][1], points[0][2],
  points[3][0], points[3][1], points[3][2]
};

GLfloat* Tetrahedron::get_vertex_data() {
  return Tetrahedron::vertex_data;
}

GLfloat* Tetrahedron::get_color_data() {
  return Tetrahedron::color_data;
}
