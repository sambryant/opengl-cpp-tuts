#ifndef SERPENSKI_HPP
#define SERPENSKI_HPP

#include "scene.hpp"
#include "object.hpp"
#include "tetrahedron.hpp"
#include "serpenski.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

Scene* serpenski_gen_scene(GLuint modelID);

#endif
