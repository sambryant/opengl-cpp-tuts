#ifndef WORLD_STATE_HPP
#define WORLD_STATE_HPP

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace std;

class WorldState {

public:
  glm::vec3 position;
  float horizontalAngle;
  float verticalAngle;
  float fov;
  float speed;
  float mouseSpeed;

  WorldState(GLuint programID, GLFWwindow* window);

  void send_state();
  void set_start_position(GLfloat x, GLfloat y, GLfloat z);
  void set_fov(GLfloat fov);

private:
  GLFWwindow* window;
  GLuint viewID;
  GLuint projectionID;
  GLuint programID;
  glm::mat4 projectionMatrix;
};

#endif